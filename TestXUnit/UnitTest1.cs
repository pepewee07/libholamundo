using LibHolaMundo2;
namespace TestXUnit
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            //Arrange
            Mensaje miMensaje = new Mensaje();
            string valorReal,valorEsperado = 'Hola Mundo';

            //Act
            miMensaje.Saludo = "Hola Mundo";
            valorReal = miMensaje.Saludar();

            //Assert
            Assert.Equal(valorEsperado, valorReal);

        }
    }
}